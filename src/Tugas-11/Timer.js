import React from 'react'
import ReactDOM from 'react-dom'

class Timer extends React.Component {
    constructor (props) {
      super(props) 
        this.state = {
          count: 1000
        }
      }
  
      render() {
        const {count} = this.state
        return (
          <div>
            <h1> {count} </h1>
          </div>
        )    
      }
  
      componentDidMount () {
        this.MyInterval = setInterval(() => {
          this.setState(prevState => ({
            count: prevState.count - 1
          }))
        }, 1000)
      }
    }

export default Timer 